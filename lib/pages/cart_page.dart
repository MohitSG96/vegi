import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grocery/models/item.dart';
import 'package:grocery/models/scoped_model/MainModel.dart';
import 'package:grocery/models/scoped_model/methods.dart';
import 'package:grocery/pages/show_products.dart';
import 'package:grocery/utils/globals.dart' as globals;
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:scoped_model/scoped_model.dart';

import 'login_screen.dart';

class CartCheckOut extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<CartCheckOut> {
  MainModel _model;
  String name, address, email, phone, city = "";
  Razorpay _razorPay;
  double amount;
  var razAmount, razOrderId, razCurrency;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _model = ScopedModel.of(context);
    name = globals.userFullName;
    address = globals.userAddress;
    email = globals.userEmail;
    phone = globals.userPhone;
    city = globals.city;
    _razorPay = new Razorpay();
    _razorPay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorPay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorPay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    sendOrder(response);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    setState(() {
      _isLoading = false;
    });
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (cxt) {
        return AlertDialog(
          title: Text("Payment Failure"),
          content: Text(response.message),
          actions: <Widget>[
            FlatButton(
              child: Text("OK"),
              onPressed: () {
                Navigator.of(cxt).pop();
              },
            )
          ],
        );
      },
    );
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    setState(() {
      _isLoading = false;
    });
    print("External Wallet Payment:");
    print(response);
  }

  @override
  void dispose() {
    super.dispose();
    _razorPay.clear();
  }

  generateRazorPayOrder() async {
    if (amount == 0 && _model.cartItem.length > 0) {
      var response = await _model.orderFreeProducts(amount);
      if (!response["error"]) {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (cxt) {
            return AlertDialog(
              title: Text("Order Placed Successfully"),
              content: Text("Your Order has been placed Successfully"),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  onPressed: () {
                    Navigator.of(cxt).pop();
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
      } else {
        setState(() {
          _isLoading = false;
        });
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (cxt) {
            return AlertDialog(
              title: Text("Order Failure"),
              content: Text("${response['message']}"),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  onPressed: () {
                    Navigator.of(cxt).pop();
                  },
                )
              ],
            );
          },
        );
      }
    } else if (amount > 0 && _model.cartItem.length > 0) {
      var response = await _model.getOrders(amount);
      if (!response["error"]) {
        razOrderId = response["id"];
        razAmount = response["amount"];
        razCurrency = response["currency"];
        razorPayCheckOut();
      } else {
        setState(() {
          _isLoading = false;
        });
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (cxt) {
            return AlertDialog(
              title: Text("Order Failure"),
              content: Text("${response['message']}"),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  onPressed: () {
                    Navigator.of(cxt).pop();
                  },
                )
              ],
            );
          },
        );
      }
    } else {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (cxt) {
          return AlertDialog(
            title: Text("Cart is Empty"),
            content: Text("Please add item in Cart"),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(cxt).pop();
                },
              )
            ],
          );
        },
      );
    }
  }

  razorPayCheckOut() {
    var options = {
//      'key': 'rzp_test_owBJQQBSzUUhTm', //--->Previous Test Key
      'key': Methods.RAZOR_LIVE,
      'amount': razAmount, //in the smallest currency sub-unit.
      'name': 'Shree Sai Suppliers',
      'order_id': razOrderId, // Generate order_id using Orders API
      'description': 'Payment Transaction for Vegi App',
      'currency': razCurrency,
      'prefill': {'contact': globals.userPhone, 'email': globals.userEmail},
    };
    _razorPay.open(options);
  }

  @override
  Widget build(BuildContext context) {
    amount = _model.cartAmount();
    return Scaffold(
      appBar: new AppBar(title: Text("Cart")),
      body: Column(
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  color: Colors.grey.shade200,
                  border: Border.all(color: Colors.black12),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.shade200,
                        offset: Offset(0.0, 2.0),
                        blurRadius: 5)
                  ]),
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(5),
                        child: Text("Deliver To",
                            style: TextStyle(
                                fontSize: 14.0, fontWeight: FontWeight.w600)),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 5, right: 5),
                        child: Text("$name",
                            style: TextStyle(
                                fontSize: 14.0, fontWeight: FontWeight.bold)),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 3, right: 3),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            color: Colors.grey.shade300),
                        alignment: Alignment.center,
                        child: Text("Home"),
                      ),
                    ],
                  ),
                ],
              )),
          Container(
            height: MediaQuery.of(context).size.height * 0.68,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: _model.cartItem.length,
              itemBuilder: (BuildContext context, int index) {
                Item item = _model.cartItem[index];
                var _selectedItem;
                if (item.price != 0) {
                  int j = item.prices.indexWhere((element) =>
                      element["nashikPrice"] == item.price.round() ||
                      element["mumbaiPrice"] == item.price.round());
                  var price = item.prices[j];
                  _selectedItem = price["_id"];
                } else {
                  _selectedItem = "0";
                }
                return StatefulBuilder(
                  builder: (context, setStateTile) {
                    int len = item.prices.length;
                    List<DropdownMenuItem> items = new List();
                    items.add(DropdownMenuItem(
                      child: Text(
                          "Rs ${item.prices[0][city.trim().toLowerCase() == "nashik" ? "nashikPrice" : "mumbaiPrice"]}" +
                              "/" +
                              "${item.prices[0]["measurementType"]}"),
                      value: "${item.prices[0]["_id"]}",
                    ));
                    if (len > 1) {
                      items.add(DropdownMenuItem(
                        child: Text(
                            "Rs ${item.prices[1][city.trim().toLowerCase() == "nashik" ? "nashikPrice" : "mumbaiPrice"]}" +
                                "/" +
                                "${item.prices[1]["measurementType"]}"),
                        value: "${item.prices[1]["_id"]}",
                      ));
                    }
                    if (len > 2) {
                      items.add(DropdownMenuItem(
                        child: Text(
                            "Rs ${item.prices[2][city.trim().toLowerCase() == "nashik" ? "nashikPrice" : "mumbaiPrice"]}" +
                                "/" +
                                "${item.prices[2]["measurementType"]}"),
                        value: "${item.prices[2]["_id"]}",
                      ));
                    }
                    if (len > 3) {
                      items.add(DropdownMenuItem(
                        child: Text(
                            "Rs ${item.prices[3][city.trim().toLowerCase() == "nashik" ? "nashikPrice" : "mumbaiPrice"]}" +
                                "/" +
                                "${item.prices[3]["measurementType"]}"),
                        value: "${item.prices[3]["_id"]}",
                      ));
                    }
                    return Container(
                      child: Card(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            ListTile(
                              title: Text("${item.name}"),
                              subtitle: Text("₹ ${item.price} /-"),
                              leading: Image.network(
                                item.image != null && item.image.isNotEmpty
                                    ? globals.API_BASE +
                                        globals.PRODUCT_IMAGE +
                                        item.image
                                    : "https://vegi.co.in/static/media/logo.2989e92e.png",
                                width: 100,
                                height: 100,
                                fit: BoxFit.cover,
                              ),
                              trailing: Column(
                                children: <Widget>[
                                  Text("Units:${item.quantity}"),
                                  SizedBox(height: 5),
                                  Text(
                                    "Amount: ${item.quantity * item.price}",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    DropdownButton(
                                      value: _selectedItem != "0"
                                          ? _selectedItem
                                          : null,
                                      hint: Text("Select..."),
                                      items: items,
                                      onChanged: (value) {
                                        _selectedItem = value;
                                        if (value != "0") {
                                          int i = item.prices.indexWhere(
                                              (element) =>
                                                  element["_id"] == value);
                                          var price = item.prices[i];
                                          if (city.toLowerCase().trim() ==
                                              "nashik") {
                                            setState(() {
                                              item.price = double.tryParse(
                                                  price["nashikPrice"].toString());
                                            });
                                          } else {
                                            setState(() {
                                              item.price = double.tryParse(
                                                  price["mumbaiPrice"].toString());
                                            });
                                          }
                                        }
                                      },
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top: 1),
                                      width: 150,
                                      child: _selectedItem != "0"
                                          ? Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                IconButton(
                                                  icon: Icon(
                                                    Icons.remove_circle,
                                                    color: Colors.red,
                                                    size: 35,
                                                  ),
                                                  onPressed: () {
                                                    setState(() {
                                                      {
                                                        if (item.quantity !=
                                                            1.0) {
                                                          item.quantity -= 1;
                                                        }
                                                      }
                                                    });
                                                  },
                                                ),
                                                Container(
                                                  color: Colors.grey.shade500,
                                                  padding: EdgeInsets.fromLTRB(
                                                      12, 0, 12, 0),
                                                  child: Text(item.quantity
                                                      .round()
                                                      .toString()),
                                                ),
                                                InkWell(
                                                  child: Icon(
                                                    Icons.add_circle,
                                                    color: Colors.green,
                                                    size: 35,
                                                  ),
                                                  onTap: () {
                                                    setState(() {
                                                      {
                                                        if (item.quantity !=
                                                            item.unit) {
                                                          item.quantity += 1;
                                                        } else {
                                                          FlutterToast.showToast(
                                                              msg:
                                                                  "Sorry! You have reached Maximum quantity limit.",
                                                              toastLength: Toast
                                                                  .LENGTH_SHORT);
                                                        }
                                                      }
                                                    });
                                                  },
                                                ),
                                              ],
                                            )
                                          : Center(),
                                    ),
                                  ],
                                ),
                                Container(
                                  width: double.maxFinite,
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        _model.cartItem.removeAt(index);
                                      });
                                    },
                                    child: Padding(
                                      padding:
                                          EdgeInsets.only(top: 5, bottom: 5),
                                      child: Text(
                                        "Remove",
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, -1),
                  blurRadius: 5,
                  color: Colors.grey.shade300)
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                "Total: ${_model.cartAmount()} ₹",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
              ),
            ),
            InkWell(
              onTap: () {
                if (globals.userId != null && globals.userId.isNotEmpty) {
                  generateRazorPayOrder();
                  setState(() {
                    _isLoading = true;
                  });
                } else {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) => LoginScreen()));
                }
              },
              child: Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.deepOrange,
                  borderRadius: BorderRadius.circular(1),
                ),
                child: Text(
                  "Place Order",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  sendOrder(razorResponse) async {
    var response = await _model.orderCart({
      "razorpay_payment_id": razorResponse.paymentId,
      "razorpay_order_id": razorResponse.orderId,
      "razorpay_signature": razorResponse.signature
    });
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (cxt) {
        return AlertDialog(
          title: Text("Payment SuccessFull"),
          content: Text("Your Payment was Successful."),
          actions: <Widget>[
            FlatButton(
              child: Text("OK"),
              onPressed: () {
                globals.catId = "*";
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            ProductsList(category: "All Items")),
                    (route) => false);
              },
            )
          ],
        );
      },
    );
  }

  DropdownMenuItem getDropDownMenuItem(val) {
    Map<String, dynamic> price = val;
    var cost = price["mumbaiPrice"].toString();
    if (globals.city.toLowerCase() == "nashik") {
      cost = price["nashikPrice"].toString();
    }
    return DropdownMenuItem(
      child: getPriceText(val),
      value: cost,
    );
  }

  Text getPriceText(val) {
    Map<String, dynamic> price = val;
    var cost = price["mumbaiPrice"].toString();
    if (globals.city.toLowerCase() == "nashik") {
      cost = price["nashikPrice"].toString();
    }
    return Text(
      "₹ " + cost + "/" + price["measurementType"],
      style:
          TextStyle(fontWeight: FontWeight.bold, color: Colors.grey.shade700),
    );
  }
}
