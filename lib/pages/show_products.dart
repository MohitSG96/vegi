import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grocery/models/category.dart';
import 'package:grocery/models/item.dart';
import 'package:grocery/models/scoped_model/MainModel.dart';
import 'package:grocery/pages/cart_page.dart';
import 'package:grocery/pages/login_screen.dart';
import 'package:grocery/pages/profile_screen.dart';
import 'package:grocery/pages/search.dart';
import 'package:grocery/utils/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProductsList extends StatefulWidget {
  final String category;
  final String subCategory;

  ProductsList({Key key, @required this.category, this.subCategory})
      : super(key: key);

  @override
  _ListProducts createState() => _ListProducts(category);
}

class _ListProducts extends State<ProductsList> {
  String category, subCategory = '', appTitle;
  Future<List<Item>> futureItems;
  List<Item> itemList;
  bool isLoading = false;
  Icon actionIcon = new Icon(Icons.person, color: Colors.white);
  MainModel _model;
  SharedPreferences _sharedPreferences;
  List<Category> listCatFree = [];

  _ListProducts(String category) {
    this.category = category;
    appTitle = category;
  }

  int itemsCount;
  double cartPrice = 0;

  @override
  void initState() {
    super.initState();
    getSharedPreferences();
    if (widget.subCategory != null && widget.subCategory.isNotEmpty) {
      subCategory = widget.subCategory;
      setState(() {
        appTitle = subCategory;
      });
    }
  }

  Future<List<Category>> getCategories() async {
    List<Category> categories = [];
    Map<String, String> headers = {
      "Content-type": "application/json",
      "Authorization": globals.userToken
    };
    try {
      var response =
          await http.get(globals.API_BASE + globals.CATEGORY, headers: headers);
      if (response.statusCode == HttpStatus.ok) {
        var resJson = json.decode(response.body);
        var error = resJson['error'];
        if (!error) {
          if (globals.freeCatId.isEmpty || globals.freeCatId == "*") {
            globals.freeCatId = globals.plan.trim();
          }
          var cat = resJson['data'];
          int index = 0;
          for (Map<String, dynamic> category in cat) {
            categories.add(Category.fromJson(category));
            index++;
          }
        } else {
          var errors = resJson["errors"];
          showDialog(
              context: context,
              builder: (context) => Text(errors.toString()),
              barrierDismissible: true);
        }
      } else if (response.statusCode == HttpStatus.forbidden) {
        var resJson = json.decode(response.body);
        var errors = resJson["errors"];
        showDialog(
            context: context,
            builder: (context) => Text(errors.toString()),
            barrierDismissible: true);
      } else {
        AlertDialog(
          title: Text("Error in Network"),
          content: Text(
              "Network Problem. Please Check Network Connection\nError:" +
                  response.statusCode.toString()),
        );
      }
    } catch (ex) {
      print(ex);
    }
    return categories;
  }

  void getSharedPreferences() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          _model = model;
          var view1, view2;
          view1 = Column(
            children: <Widget>[
              FutureBuilder(
                future: getCategories(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasData) {
                      return createCategoryHeader(
                          context, snapshot.data, false);
                    } else {
                      return Center();
                    }
                  } else {
                    return Center();
                  }
                },
              ),
              FutureBuilder(
                future: model.getProducts(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return Center(
                        child: new CircularProgressIndicator(),
                      );
                      break;
                    default:
                      if (snapshot.hasData) {
                        return Container(
                            child: createProductsView(
                                context, snapshot.data, false));
                      } else {
                        return Container(
                          alignment: Alignment.center,
                          child: Text(
                            "Something Went Wrong!",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.bold),
                          ),
                        );
                      }
                  }
                },
              )
            ],
          );
          view2 = Column(
            children: <Widget>[
              FutureBuilder(
                future: getCategories(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasData) {
                      return createCategoryHeader(context, snapshot.data, true);
                    } else {
                      return Center();
                    }
                  } else {
                    return Center();
                  }
                },
              ),
              FutureBuilder(
                future: model.getProducts(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return Center(
                        child: new CircularProgressIndicator(),
                      );
                      break;
                    default:
                      if (snapshot.hasData) {
                        return Container(
                            child: createProductsView(
                                context, snapshot.data, true));
                      } else {
                        return Container(
                          alignment: Alignment.center,
                          child: Text(
                            "Something Went Wrong!",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.bold),
                          ),
                        );
                      }
                  }
                },
              )
            ],
          );
          return Scaffold(
            appBar: AppBar(
              elevation: 1,
              title: Text("Veggies"),
              actions: <Widget>[
                IconButton(
                  icon: this.actionIcon,
                  onPressed: () {
                    if (globals.userName == null || globals.userName.isEmpty)
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => LoginScreen()));
                    else
                      Navigator.of(context)
                          .push(MaterialPageRoute(
                              builder: (context) => ProfilePage()))
                          .then((value) {
                        setState(() {});
                      });
                  },
                ),
                IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () async {
                      await showSearch(
                        context: context,
                        delegate: SearchItem(
                          model: this._model,
                          listCatFree: listCatFree,
                        ),
                      );
                    }),
                Stack(
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.shopping_cart, color: Colors.white),
                        onPressed: () {
                          if (globals.userName == null ||
                              globals.userName.isEmpty) {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    LoginScreen()));
                            FlutterToast.showToast(msg: "Please Login First");
                          } else
                            Navigator.of(context)
                                .push(MaterialPageRoute(
                                    builder: (context) => CartCheckOut()))
                                .then((value) {
                              setState(() {});
                            });
                        }),
                    new Positioned(
                      right: 2,
                      top: 2,
                      child: new Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(1),
                        decoration: new BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        constraints: BoxConstraints(
                          minWidth: 20,
                          minHeight: 20,
                        ),
                        child: Text(
                          "${_model.getItemCount()}",
                          style: TextStyle(color: Colors.white, fontSize: 12),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ],
                ),
              ],
              bottom: TabBar(tabs: [
                Tab(
                  text: "Normal Category",
                ),
                Tab(
                  text: "Plans Category",
                ),
              ]),
            ),
            body: TabBarView(children: [
              SingleChildScrollView(
                child: view1,
              ),
              SingleChildScrollView(
                child: view2,
              ),
            ]),
          );
        },
      ),
    );
  }

  createCategoryHeader(context, data, isPlan) {
    int index = 0;
    List<Category> listCat = [];
    for (var d in data) {
      if (d.isForFree == isPlan) {
        if (isPlan) {
          if (index == 0 && globals.freeCatId.isEmpty ||
              globals.freeCatId == "*") globals.freeCatId = d.id;
        } else {
          if (index == 0 && globals.catId.isEmpty || globals.catId == "*")
            globals.catId = d.id;
        }
        listCatFree.add(d);
        listCat.add(d);
        index++;
      }
    }
    return Container(
      height: 120,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: listCat.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          Category cat = listCat[index];
          var categoryIcon = cat.image != null && cat.image.trim().isNotEmpty
              ? Container(
                  width: 50,
                  height: 50,
                  child: Image.network(
                      globals.API_BASE + globals.CATEGORY_IMAGE + cat.image),
                )
              : Container(
                  width: 50,
                  height: 50,
                  child: FadeInImage(
                    placeholder: AssetImage(getCatImage(cat.title)),
                    image: cat.image != null && cat.image.trim().isNotEmpty
                        ? NetworkImage(globals.API_BASE +
                            globals.CATEGORY_IMAGE +
                            cat.image)
                        : AssetImage(getCatImage(cat.title)),
                  ),
                );
          return Container(
            width: 70,
            padding: EdgeInsets.all(2),
            alignment: Alignment.topCenter,
            child: InkWell(
              onTap: () {
                setState(() {
                  category = cat.title;
                  if (isPlan) {
                    globals.freeCatId = cat.id;
                  } else {
                    globals.catId = cat.id;
                  }
                });
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  categoryIcon,
                  Text(
                    "${cat.title}",
                    textAlign: TextAlign.center,
                  ),
                  isPlan
                      ? globals.freeCatId == cat.id
                          ? SizedBox(
                              height: 2,
                              child: Container(color: Colors.red),
                            )
                          : SizedBox(height: 2)
                      : globals.catId == cat.id
                          ? SizedBox(
                              height: 2,
                              child: Container(color: Colors.red),
                            )
                          : SizedBox(height: 2)
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  createProductsView(BuildContext context, data, isFree) {
    itemList = data;
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: itemList.length,
      itemBuilder: (context, index) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setStateTile) {
            Item item;
            String imageUrl =
                "https://vegi.co.in/static/media/logo.2989e92e.png";
            if (globals.catId == "*") {
              item = itemList[index];
            } else {
              if (isFree) {
                if (itemList[index].catId == globals.freeCatId) {
                  item = itemList[index];
                } else {
                  return Center();
                }
              } else {
                if (itemList[index].catId == globals.catId) {
                  item = itemList[index];
                } else {
                  return Center();
                }
              }
            }
            if (item.image != null && item.image.isNotEmpty) {
              if (item.image.contains("https://") ||
                  item.image.contains("http://"))
                imageUrl = item.image;
              else
                imageUrl =
                    globals.API_BASE + globals.PRODUCT_IMAGE + item.image;
            }
            if (item.prices == null || item.prices.length == 0) {
              return Center();
            }
            Map<String, dynamic> price =
                item.prices != null && item.prices.length != 0
                    ? item.prices[0]
                    : {
                        "mumbaiPrice": 0,
                        "nashikPrice": 0,
                      };
            var cost = price["mumbaiPrice"];
            if (item.catId == globals.plan) {
              if (checkFreeProduct(item)) {
                cost = 0;
              }
            } else if (globals.city.toLowerCase() == "nashik") {
              cost = price["nashikPrice"];
            }
            return Card(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        width: 150,
                        height: 150,
                        child: Image.network(
                          imageUrl,
                          width: 150,
                          height: 150,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Container(
                          decoration: BoxDecoration(
                              border: Border(left: BorderSide(width: 1.5))),
                          child: Column(
                            children: [
                              Container(
                                width: 200,
                                child: Text(
                                  item.name,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.red,
                                    fontSize: 18,
                                    decoration: TextDecoration.underline,
                                    decorationStyle: TextDecorationStyle.double,
                                  ),
                                ),
                              ),
                              Container(
                                child: Container(
                                  margin: EdgeInsets.only(top: 3, bottom: 5),
                                  child: item.prices != null &&
                                          item.prices.length != 0
                                      ? getPrices(item)
                                      : printItem(item),
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 1),
                    width: 125,
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: MaterialButton(
                        child: item.unit != null && item.unit > 0
                            ? Text("Add to Cart")
                            : Text("Out of Stock"),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                          side: BorderSide(color: Colors.redAccent),
                        ),
                        onPressed: item.unit != null && item.unit > 0
                            ? () {
                                if (isFree) {
                                  if (item.catId == globals.plan) {
                                    this.setState(() {
                                      _model.addToCart(item, cost);
                                      FlutterToast.showToast(
                                          msg: "Item Added in Cart",
                                          toastLength: Toast.LENGTH_LONG);
                                    });
                                  } else {
                                    FlutterToast.showToast(
                                        msg:
                                            "You can only add your plan products or normal products",
                                        gravity: ToastGravity.CENTER,
                                        toastLength: Toast.LENGTH_LONG);
                                  }
                                } else {
                                  if (checkIfFreeProductInCart()) {
                                    FlutterToast.showToast(
                                        msg:
                                            "Normal products cannot be added with your plan product",
                                        gravity: ToastGravity.CENTER,
                                        toastLength: Toast.LENGTH_LONG);
                                  } else {
                                    this.setState(() {
                                      _model.addToCart(item, cost);
                                      FlutterToast.showToast(
                                          msg: "Item Added in Cart",
                                          toastLength: Toast.LENGTH_LONG);
                                    });
                                  }
                                }
                              }
                            : null,
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  Widget printItem(item) {
    print(item.title);
    return Center();
  }

  checkIfFreeProductInCart() {
    int index =
        _model.cartItem.indexWhere((element) => element.catId == globals.plan);
    if (index >= 0) {
      return true;
    } else {
      return false;
    }
  }

  getPrices(Item item) {
    var prices = item.prices;
    int length = prices.length;
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        getPriceOrZero(item),
        length > 1 ? getPriceText(prices[1]) : Center(),
        length > 2 ? getPriceText(prices[2]) : Center(),
        length > 3 ? getPriceText(prices[3]) : Center(),
      ],
    );
  }

  getPriceOrZero(Item item) {
    Map<String, dynamic> price = item.prices[0];
    var cost = price["mumbaiPrice"].toString();
    if (item.catId == globals.plan) {
      if (checkFreeProduct(item)) {
        cost = "0";
      }
    } else if (globals.city.toLowerCase() == "nashik") {
      cost = price["nashikPrice"].toString();
    }
    return Text(
      "₹ " + cost + "/" + price["measurementType"],
      style:
          TextStyle(fontWeight: FontWeight.bold, color: Colors.grey.shade700),
    );
  }

  bool checkFreeProduct(Item item) {
    var index = listCatFree.indexWhere((c) => c.id == item.catId);
    if (index < 0) {
      return false;
    } else {
      var free = listCatFree[index].freeProducts;
      if (_model.getFreeProductCount() < free) {
        return true;
      } else {
        return false;
      }
    }
  }

  getPriceText(val) {
    Map<String, dynamic> price = val;
    var cost = price["mumbaiPrice"].toString();
    if (globals.city.toLowerCase() == "nashik") {
      cost = price["nashikPrice"].toString();
    }
    return Text(
      "₹ " + cost + "/" + price["measurementType"],
      style:
          TextStyle(fontWeight: FontWeight.bold, color: Colors.grey.shade700),
    );
  }

  getCatImage(name) {
    if (name.toString().toLowerCase().contains("vegeteble")) {
      return "assets/exotic_veg.webp";
    }
    if (name.toString().toLowerCase().contains("cutting")) {
      return "assets/cutting.webp";
    }
    if (name.toString().toLowerCase().contains("fruit")) {
      return "assets/fruits.webp";
    }
    if (name.toString().toLowerCase().contains("green")) {
      return "assets/green_vegi.webp";
    }
    if (name.toString().toLowerCase().contains("lettuce")) {
      return "assets/lettuce.webp";
    }
    if (name.contains("sprouts")) {
      return "assets/sprouts.webp";
    }
    if (name.toString().toLowerCase().contains("seasonal")) {
      return "assets/seasonal.webp";
    }
    if (name.toString().toLowerCase().contains("featured")) {
      return "assets/featured.webp";
    }
    if (name.toString().toLowerCase().contains("non veg")) {
      return "assets/non_veg.webp";
    }

    return "assets/fruit_vegi.webp";
  }
}
