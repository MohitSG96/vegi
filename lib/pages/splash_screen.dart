import 'dart:async';
import 'package:flutter/material.dart';
import 'package:grocery/models/scoped_model/MainModel.dart';
import 'package:grocery/pages/login_screen.dart';
import 'package:grocery/pages/show_products.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:grocery/utils/globals.dart' as globals;

class SplashScreen extends StatefulWidget {
  final Color backgroundColor = Colors.white;
  final TextStyle styleTextUnderTheLoader = TextStyle(
      fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.black);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences sharedPreferences;
  String _versionName = 'V1.0';
  final splashDelay = 5;
  var load;

  @override
  void initState() {
    super.initState();
    checkLogin();
  }

  _loadWidget() async {
    var _duration = Duration(seconds: splashDelay);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (BuildContext context) => load));
  }

  checkLogin() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("user_id") != null &&
        sharedPreferences.getString("user_id").isNotEmpty) {
      globals.userId = sharedPreferences.getString("user_id");
      globals.userToken = sharedPreferences.getString("token");
      globals.userName = sharedPreferences.getString("user_name");
      globals.userEmail = sharedPreferences.getString("email");
      globals.userAddress = sharedPreferences.getString("address");
      globals.userPhone = sharedPreferences.getString("phone");
      globals.userFullName = sharedPreferences.getString("name");
      globals.plan = sharedPreferences.getString("plan");
      globals.city = sharedPreferences.getString("city");
    }
    if (sharedPreferences.getBool("welcome") != null &&
        sharedPreferences.getBool("welcome")) {
      load = LoginScreen();
      _loadWidget();
    } else {
      globals.catId = "*";
      load = ProductsList(category: "Vegi App");
      _loadWidget();
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return Scaffold(
        body: InkWell(
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Container(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.network(
                          'https://vegi.co.in/static/media/logo.2989e92e.png',
                          height: 300,
                          width: 300,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                        ),
                      ],
                    )),
                  ),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        CircularProgressIndicator(),
                        Container(
                          height: 10,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Spacer(),
                              Text(_versionName),
                              Spacer(
                                flex: 4,
                              ),
                              Text('androing'),
                              Spacer(),
                            ])
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    });
  }
}
