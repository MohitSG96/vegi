import 'package:flutter/material.dart';
import 'package:grocery/models/scoped_model/MainModel.dart';
import 'package:grocery/pages/all_orders.dart';
import 'package:grocery/utils/globals.dart' as globals;
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'cart_page.dart';

// ignore: must_be_immutable
class ProfilePage extends StatelessWidget {
  String userName, name, email, phone, address;
  SharedPreferences preferences;

  String city;

  showUserProfile(context) {
    return SingleChildScrollView(
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: Container(
                width: 140,
                height: 140,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                      image: ExactAssetImage("assets/profile.webp"),
                      fit: BoxFit.cover,
                    )),
              ),
            ),
            Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                child: ListTile(
                  leading: SizedBox(
                    width: 100,
                    child: Text(
                      "Name:",
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                  title: Text(
                    name,
                    style: TextStyle(fontFamily: 'BalooBhai', fontSize: 20.0),
                  ),
                )),
            Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                child: ListTile(
                  leading: SizedBox(
                    width: 100,
                    child: Text(
                      "Phone:",
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                  title: Text(
                    phone,
                    style: TextStyle(fontFamily: 'BalooBhai', fontSize: 20.0),
                  ),
                )),
            Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                child: ListTile(
                  leading: SizedBox(
                    width: 100,
                    child: Text(
                      "UserName:",
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                  title: Text(
                    userName,
                    style: TextStyle(fontFamily: 'BalooBhai', fontSize: 20.0),
                  ),
                )),
            Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                child: ListTile(
                  leading: SizedBox(
                      width: 100,
                      child: Text(
                        "Email:",
                        style: TextStyle(fontSize: 20.0),
                      )),
                  title: Text(
                    email,
                    style: TextStyle(fontFamily: 'BalooBhai', fontSize: 20.0),
                  ),
                )),
            Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                child: ListTile(
                  leading: SizedBox(
                    width: 100,
                    child: Text(
                      "Address:",
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                  title: Text(
                    address,
                    style: TextStyle(fontFamily: 'BalooBhai', fontSize: 20.0),
                  ),
                )),
            Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                child: ListTile(
                  leading: SizedBox(
                    width: 100,
                    child: Text(
                      "City:",
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                  title: Text(
                    city.isEmpty ? "NA" : city,
                    style: TextStyle(fontFamily: 'BalooBhai', fontSize: 20.0),
                  ),
                )),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                MaterialButton(
                  padding: EdgeInsets.all(10),
                  color: Colors.teal,
                  child: Text(
                    "Log Out",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    globals.userId = "";
                    globals.userToken = "";
                    globals.userName = "";
                    globals.userEmail = "";
                    globals.userAddress = "";
                    globals.userPhone = "";
                    globals.userFullName = "";
                    globals.city = "";
                    preferences.clear();
                    Navigator.of(context).pop();
                  },
                ),
                MaterialButton(
                  padding: EdgeInsets.all(10),
                  color: Colors.teal,
                  child: Text(
                    "Previous Orders",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => OrdersPage()));
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    getSharedInstance();
    MainModel _model = ScopedModel.of(context);
    return Scaffold(
        backgroundColor: Colors.green.shade50,
        appBar: new AppBar(
          title: Text("Profile"),
          actions: <Widget>[
            Stack(
              children: <Widget>[
                IconButton(
                    icon: Icon(Icons.shopping_cart, color: Colors.white),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => CartCheckOut()));
                    }),
                new Positioned(
                  right: 2,
                  top: 2,
                  child: new Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(1),
                    decoration: new BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 20,
                      minHeight: 20,
                    ),
                    child: Text(
                      "${_model.getItemCount()}",
                      style: TextStyle(color: Colors.white, fontSize: 12),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
        body: Stack(
          children: <Widget>[
            Center(
              child: Container(
                decoration: BoxDecoration(),
              ),
            ),
            FutureBuilder(
              future: SharedPreferences.getInstance(),
              builder: (BuildContext context,
                  AsyncSnapshot<SharedPreferences> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                    return Center(child: CircularProgressIndicator());
                  default:
                    if (snapshot.hasData) {
                      SharedPreferences preferences = snapshot.data;
                      name = preferences.getString("name");
                      phone = preferences.getString("phone");
                      address = preferences.getString("address");
                      email = preferences.getString("email");
                      userName = preferences.getString("user_name");
                      city = preferences.getString("city") ?? "";
                      return Container(child: showUserProfile(context));
                    } else {
                      return Container(
                        child: Text("Something Went Wong"),
                      );
                    }
                }
              },
            )
          ],
        ));
  }

  getSharedInstance() async {
    preferences = await SharedPreferences.getInstance();
  }
}
