import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:grocery/models/item.dart';
import 'package:grocery/models/orderProduct.dart';
import 'package:grocery/models/previousOrder.dart';
import 'package:grocery/models/scoped_model/MainModel.dart';
import 'package:grocery/pages/order_detail.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;
import 'package:grocery/utils/globals.dart' as globals;

class OrdersPage extends StatefulWidget {
  @override
  _OrderState createState() => _OrderState();
}

class _OrderState extends State<OrdersPage> {
  MainModel _model;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    _model = ScopedModel.of(context);
  }

  previousOrders() async {
    List<PreviousOrder> previousProducts = [];

    Map<String, String> headers = {"Authorization": globals.userToken};
    try {
      var response = await http.get(globals.API_BASE + globals.GET_ORDERS,
          headers: headers);
      if (response.statusCode == HttpStatus.ok) {
        var resJson = json.decode(response.body);
        var error = resJson['error'];
        if (!error) {
          var ordersMap = resJson['data'];
          previousProducts = [];
          double total = 0.0;
          for (Map<String, dynamic> order in ordersMap) {
            List<OrderProduct> products = [];
            for (Map<String, dynamic> product in order['products']) {
              products.add(OrderProduct.fromJson(product));
              total += double.tryParse(product['price'].toString());
            }
            previousProducts.add(PreviousOrder.fromJson(order, total, products));
          }
        }
      } else if (response.statusCode == HttpStatus.forbidden) {
        var resJson = json.decode(response.body);
        var errors = resJson['errors'];
        String msg;
        for (var param in errors) {
          msg = param['msg'];
        }
        showDialog(
          context: context,
          builder: (BuildContext bc) {
            return AlertDialog(
              title: new Text('Error!'),
              content: Text('Error: ' + msg),
              actions: <Widget>[
                new FlatButton(
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      } else {
        var data = jsonDecode(response.body);
        var msg = "Something Went Wrong\nPlease try again after some time!";
        if (response.statusCode == HttpStatus.badRequest) {
          var errors = data['errors'];
          for (var param in errors) {
            msg = param['msg'];
          }
          setState(() {
            _isLoading = false;
          });
        } else {
          print(response.body);
        }
        showDialog(
          context: context,
          builder: (BuildContext bc) {
            return AlertDialog(
              title: new Text('Error!'),
              content: Text('Error: ' + msg),
              actions: <Widget>[
                new FlatButton(
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    } catch (ex) {
      print(ex);
      String msg =
          "Something Went wrong.\nIf You see this error  frequently then contact to Vegi App support.";
      showDialog(
        context: context,
        builder: (BuildContext bc) {
          return AlertDialog(
            title: new Text('Error!'),
            content: Text('Error: ' + msg),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Ok"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    return previousProducts;
  }

  showOrdersList(lists) {
    List<PreviousOrder> orders = lists;
    if (orders.length > 0) {
      return ListView.builder(
        itemCount: orders.length,
        itemBuilder: (context, index) {
          PreviousOrder item = orders[index];
          DateTime dateTime = DateTime.tryParse(item.orderDateTime);
          String date = "${dateTime.day}/${dateTime.month}/${dateTime.year}";
          return Container(
            child: InkWell(
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    ListTile(
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Order Id:  ${item.id}"),
                          Text("Order Place on:\t $date", textAlign: TextAlign.start,),
                        ],
                      ),
                      subtitle:
                          Text("₹ ${item.amount} /-    Status: ${item.status}"),
                      trailing: Icon(
                          Icons.arrow_right,
                          color: Colors.black12,
                          size: 25,
                        ),
                    ),
                  ],
                ),
              ),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => OrderDetails(order: item)));
              },
            ),
          );
        },
      );
    } else {
      return Center(
        child: Text(
          "No Orders to Show here",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              color: Colors.blueGrey),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("Previous Orders"),
      ),
      body: FutureBuilder(
        future: previousOrders(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
              break;
            default:
              if (snapshot.hasData) {
                return showOrdersList(snapshot.data);
              } else {
                return Center(
                  child: Text(
                    "Something went wrong.\nPlease Come back after sometime Again.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.blueGrey),
                  ),
                );
              }
          }
        },
      ),
    );
  }
}
