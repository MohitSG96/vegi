import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grocery/models/category.dart';
import 'package:grocery/models/item.dart';
import 'package:grocery/models/scoped_model/MainModel.dart';
import 'package:grocery/utils/globals.dart' as globals;

class SearchItem extends SearchDelegate {
  MainModel model;
  List<Category> listCatFree = [];

  SearchItem({@required this.model, @required this.listCatFree})
      : super(searchFieldLabel: "Search All Products");

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query.length < 3) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text(
              "Search term must be longer than two letters.",
            ),
          )
        ],
      );
    } else {
      return FutureBuilder(
        future: model.searchProduct(query),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(child: CircularProgressIndicator()),
              ],
            );
          } else if (snapshot.hasError) {
            return Column(
              children: <Widget>[
                Text(
                  "${snapshot.error}",
                ),
              ],
            );
          } else {
            var result = snapshot.data;
            return createProductsView(context, result);
          }
        },
      );
    }
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Column();
  }

  createProductsView(BuildContext context, data) {
    var result = data;
    if (result["error"]) {
      return Column(
        children: <Widget>[
          Text(
            "${data["data"]}",
          ),
        ],
      );
    }
    List<Item> itemList = result["data"];
    if (itemList.length == 0) {
      return Column(
        children: <Widget>[
          Text(
            "No Results Found.",
          ),
        ],
      );
    }
    return ListView.builder(
//      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: itemList.length,
      itemBuilder: (context, index) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setStateTile) {
            Item item = itemList[index];
            String imageUrl =
                "https://vegi.co.in/static/media/logo.2989e92e.png";
            /*if (globals.catId == "*") {
              item = itemList[index];
            } else {
              if (isFree) {
                if (itemList[index].catId == globals.freeCatId) {
                  item = itemList[index];
                } else {
                  return Center();
                }
              } else {
                if (itemList[index].catId == globals.catId) {
                  item = itemList[index];
                } else {
                  return Center();
                }
              }
            }*/
            if (item.image != null && item.image.isNotEmpty) {
              if (item.image.contains("https://") ||
                  item.image.contains("http://"))
                imageUrl = item.image;
              else
                imageUrl =
                    globals.API_BASE + globals.PRODUCT_IMAGE + item.image;
            }
            if (item.prices == null || item.prices.length == 0) {
              return Center();
            }
            Map<String, dynamic> price =
                item.prices != null && item.prices.length != 0
                    ? item.prices[0]
                    : {
                        "mumbaiPrice": 0,
                        "nashikPrice": 0,
                      };
            var cost = price["mumbaiPrice"];
            if (item.catId == globals.plan) {
              if (checkFreeProduct(item)) {
                cost = 0;
              }
            } else if (globals.city.toLowerCase() == "nashik") {
              cost = price["nashikPrice"];
            }
            return Card(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        width: 150,
                        height: 150,
                        child: Image.network(
                          imageUrl,
                          width: 150,
                          height: 150,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Container(
                          decoration: BoxDecoration(
                              border: Border(left: BorderSide(width: 1.5))),
                          child: Column(
                            children: [
                              Container(
                                width: 200,
                                child: Text(
                                  item.name,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.red,
                                    fontSize: 18,
                                    decoration: TextDecoration.underline,
                                    decorationStyle: TextDecorationStyle.double,
                                  ),
                                ),
                              ),
                              Container(
                                child: Container(
                                  margin: EdgeInsets.only(top: 3, bottom: 5),
                                  child: item.prices != null &&
                                          item.prices.length != 0
                                      ? getPrices(item)
                                      : printItem(item),
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 1),
                    width: 125,
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: MaterialButton(
                        child: item.unit != null && item.unit > 0
                            ? Text("Add to Cart")
                            : Text("Out of Stock"),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                          side: BorderSide(color: Colors.redAccent),
                        ),
                        onPressed: item.unit != null && item.unit > 0
                            ? () {
                                if (checkIfFreeProductInCart()) {
                                  FlutterToast.showToast(
                                      msg:
                                          "Normal products cannot be added with your plan product",
                                      gravity: ToastGravity.CENTER,
                                      toastLength: Toast.LENGTH_LONG);
                                } else {
                                  model.addToCart(item, cost);
                                  FlutterToast.showToast(
                                      msg: "Item Added in Cart",
                                      toastLength: Toast.LENGTH_LONG);
                                }
                              }
                            : null,
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  Widget printItem(item) {
    print(item.title);
    return Center();
  }

  checkIfFreeProductInCart() {
    int index =
        model.cartItem.indexWhere((element) => element.catId == globals.plan);
    if (index >= 0) {
      return true;
    } else {
      return false;
    }
  }

  getPrices(Item item) {
    var prices = item.prices;
    int length = prices.length;
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        getPriceOrZero(item),
        length > 1 ? getPriceText(prices[1]) : Center(),
        length > 2 ? getPriceText(prices[2]) : Center(),
        length > 3 ? getPriceText(prices[3]) : Center(),
      ],
    );
  }

  getPriceOrZero(Item item) {
    Map<String, dynamic> price = item.prices[0];
    var cost = price["mumbaiPrice"].toString();
    if (item.catId == globals.plan) {
      if (checkFreeProduct(item)) {
        cost = "0";
      }
    } else if (globals.city.toLowerCase() == "nashik") {
      cost = price["nashikPrice"].toString();
    }
    return Text(
      "₹ " + cost + "/" + price["measurementType"],
      style:
          TextStyle(fontWeight: FontWeight.bold, color: Colors.grey.shade700),
    );
  }

  bool checkFreeProduct(Item item) {
    var index = listCatFree.indexWhere((c) => c.id == item.catId);
    if (index < 0) {
      return false;
    } else {
      var free = listCatFree[index].freeProducts;
      if (model.getFreeProductCount() < free) {
        return true;
      } else {
        return false;
      }
    }
  }

  getPriceText(val) {
    Map<String, dynamic> price = val;
    var cost = price["mumbaiPrice"].toString();
    if (globals.city.toLowerCase() == "nashik") {
      cost = price["nashikPrice"].toString();
    }
    return Text(
      "₹ " + cost + "/" + price["measurementType"],
      style:
          TextStyle(fontWeight: FontWeight.bold, color: Colors.grey.shade700),
    );
  }
}
