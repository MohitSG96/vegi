import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grocery/models/plan.dart';
import 'package:grocery/models/scoped_model/MainModel.dart';
import 'package:grocery/models/scoped_model/methods.dart';
import 'package:grocery/pages/login_screen.dart';
import 'package:grocery/utils/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _panNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _permanentAddressController =
      TextEditingController();
  final TextEditingController _correspondingAddressController =
      TextEditingController();
  final TextEditingController _executiveController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _alterPhoneController = TextEditingController();
  bool _isInvalidCredential = false, _isLoading = false;
  var alreadyExist = "", city = "", selectedPlan = "", fromList = -1;
  var selectedPlanId = "";
  int amount;
  SharedPreferences sharedPreferences;
  Razorpay _razorPay;
  var razAmount, razOrderId, razCurrency;
  MainModel _model;

  @override
  void initState() {
    super.initState();
    _loadShared();
    _model = ScopedModel.of(context);
    _razorPay = new Razorpay();
    _razorPay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorPay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorPay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  getPlanList() async {
    List<Plan> plans = new List();
    http.Response response;
    try {
      response = await http.get(globals.API_BASE + globals.PLAN);
      if (response.statusCode == HttpStatus.ok) {
        var data = jsonDecode(response.body);
        if (!data["error"]) {
          var mapPlan = data['data'];
          for (Map<String, dynamic> plan in mapPlan) {
            plans.add(Plan.fromJson(plan));
          }
        } else {
          var msg = "Something Went Wrong\nPlease try again after some time!";
          showDialog(
            context: context,
            builder: (BuildContext bc) {
              return AlertDialog(
                title: new Text('Error!'),
                content: Text('Error: ' + msg),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text("Ok"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        }
      } else {
        var data = jsonDecode(response.body);
        var msg = "Something Went Wrong\nPlease try again after some time!";
        if (response.statusCode == HttpStatus.badRequest) {
          var errors = data['errors'];
          for (var param in errors) {
            msg = param['msg'];
          }
        } else if (response.statusCode == HttpStatus.forbidden) {
          var errors = data['errors'];
          for (var param in errors) {
            msg = param['msg'];
          }
        } else {
          print(response.body);
        }
        showDialog(
          context: context,
          builder: (BuildContext bc) {
            return AlertDialog(
              title: new Text('Error!'),
              content: Text('Error: ' + msg),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    } catch (ex) {
      String msg =
          "Something Went wrong.\nIf You see this error  frequently then contact to Vegi App support.";
      showDialog(
        context: context,
        builder: (BuildContext bc) {
          return AlertDialog(
            title: new Text('Error!'),
            content: Text('Error: ' + msg),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Ok"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    return plans;
  }

  showPlans(data) {
    List<Plan> plans = data;
    return StatefulBuilder(
      builder:
          (BuildContext context, void Function(void Function()) innerState) {
        return plans.length > 0
            ? DropdownButton(
                items: createDropDownItems(plans),
                value: selectedPlanId.isNotEmpty ? selectedPlanId : null,
                hint: Text("Select a Plan"),
                isExpanded: true,
                onChanged: (value) {
                  innerState(() {
                    selectedPlanId = value;
                    var index = plans.indexWhere(
                        (p) => p.id == value.toString().substring(1));
                    if (index >= 0) {
                      selectedPlan = value.toString().substring(1);
                      var c = value.toString().substring(0, 1);
                      switch (c) {
                        case "0":
                          amount = plans[index].weeklyMumbaiPrice;
                          break;
                        case "1":
                          amount = plans[index].monthlyMumbaiPrice;
                          break;
                        case "2":
                          amount = plans[index].weeklyNashilPrice;
                          break;
                        case "3":
                          amount = plans[index].monthlyNashikPrice;
                          break;
                      }
                    } else {
                      selectedPlan = "";
                    }
                  });
                })
            : Center(
                child: Text(
                "No Plans Available",
                textAlign: TextAlign.center,
              ));
      },
    );
  }

  List<DropdownMenuItem> createDropDownItems(List<Plan> plans) {
    List<DropdownMenuItem> menuItems = [];
    for (Plan plan in plans) {
      int i = 0;
      if(plan.weeklyNashilPrice > 0) {
        menuItems.add(DropdownMenuItem(
          child: Text("${plan.title} at ${plan.weeklyMumbaiPrice} INR"),
          value: "$i${plan.id}",
        ));
      }
      i++;

      if(plan.monthlyMumbaiPrice > 0) {
        menuItems.add(DropdownMenuItem(
          child: Text("${plan.title} at ${plan.monthlyMumbaiPrice} INR"),
          value: "$i${plan.id}",
        ));
      }
      i++;

      if(plan.weeklyNashilPrice > 0) {
        menuItems.add(DropdownMenuItem(
          child: Text("${plan.title} at ${plan.weeklyNashilPrice} INR"),
          value: "$i${plan.id}",
        ));
      }
      i++;

      if(plan.monthlyNashikPrice > 0) {
        menuItems.add(DropdownMenuItem(
          child: Text("${plan.title} at ${plan.monthlyNashikPrice} INR"),
          value: "$i${plan.id}",
        ));
      }
    }
    return menuItems;
  }

  signUp(PaymentSuccessResponse razorRes) async {
    var password = _passwordController.text;
    _passwordController.text = "";
    setState(() {
      _isLoading = true;
    });
    var body = {
      "response": {
        "razorpay_payment_id": razorRes.paymentId,
        "razorpay_order_id": razorRes.orderId,
        "razorpay_signature": razorRes.signature,
      },
      "name": _fullNameController.text,
      "email": _emailController.text,
      "phone": _phoneController.text,
      "permanentAddress": _permanentAddressController.text,
      "correspondedAddress": _correspondingAddressController.text,
      "address": _permanentAddressController.text,
      "alternatePhone": _alterPhoneController.text,
      "city": city,
      "panNo": _panNameController.text,
      "password": password,
      "amount": amount ?? null,
    };
    if (selectedPlan.isNotEmpty) {
      body.addAll({"plan": selectedPlan, "permissions": "member"});
    }
    http.Response response;
    try {
      response = await http.post(globals.API_BASE + globals.SIGNUP,
          headers: {"Content-Type": "application/json"},
          body: jsonEncode(body));
      if (response.statusCode == HttpStatus.ok) {
        var data = jsonDecode(response.body);
        if (!data["error"]) {
          setState(() {
            _isLoading = false;
            _isInvalidCredential = false;
          });
          FlutterToast.showToast(
            msg: "Registered Successfully\nPlease Login Now",
            toastLength: Toast.LENGTH_LONG,
          );
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => LoginScreen()),
              (route) => false);
          return true;
        } else {
          var errors = data['errors'];
          for (var param in errors) {
            alreadyExist = param["param"];
          }
          setState(() {
            _isLoading = false;
            _isInvalidCredential = true;
          });
          return false;
        }
      } else {
        var data = jsonDecode(response.body);
        var msg = "Something Went Wrong\nPlease try again after some time!";
        if (response.statusCode == HttpStatus.badRequest) {
          var errors = data['errors'];
          for (var param in errors) {
            alreadyExist = param["param"];
            msg = param['msg'];
          }
          setState(() {
            _isLoading = false;
            _isInvalidCredential = true;
          });
        } else {
          print(response.body);
        }

        showDialog(
          context: context,
          builder: (BuildContext bc) {
            return AlertDialog(
              title: new Text('Error!'),
              content: Text('Error: ' + msg),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
        return false;
      }
    } catch (ex) {
      print(ex);
      return false;
    }
  }

  bool validateEmail(email) {
    String p =
        r"[a-z`0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
    RegExp regex = new RegExp(p);
    return regex.hasMatch(email);
  }

  void _loadShared() async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool("welcome", false);
  }

  generateRazorPayOrder() async {
    var response = await _model.getPayment(100, _emailController.text, _phoneController.text);
    if (!response["error"]) {
      razOrderId = response["id"];
      razAmount = response["amount"];
      razCurrency = response["currency"];
      razorPayCheckOut();
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (cxt) {
          return AlertDialog(
            title: Text("${response['title']}"),
            content: Text("${response['message']}"),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(cxt).pop();
                },
              )
            ],
          );
        },
      );
    }
  }

  razorPayCheckOut() {
    var options = {
//      'key': 'rzp_test_owBJQQBSzUUhTm', //--->Previous Test Key
      'key': Methods.RAZOR_LIVE,
      'amount': razAmount, //in the smallest currency sub-unit.
      'name': 'Shree Sai Suppliers',
      'order_id': razOrderId, // Generate order_id using Orders API
      'description': 'Payment Transaction for Vegi App',
      'currency': razCurrency,
      'prefill': {
        'contact': _phoneController.text,
        'email': _emailController.text
      },
      "image": "http://vegi.co.in/static/media/logo11.ee474a7e.png",
    };
    _razorPay.open(options);
  }

  sendSignUpUser(razorResponse) async {
    var response = await signUp(razorResponse);
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    sendSignUpUser(response);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    setState(() {
      _isLoading = false;
    });
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (cxt) {
        return AlertDialog(
          title: Text("Payment Failure"),
          content: Text(response.message),
          actions: <Widget>[
            FlatButton(
              child: Text("OK"),
              onPressed: () {
                Navigator.of(cxt).pop();
              },
            )
          ],
        );
      },
    );
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    setState(() {
      _isLoading = false;
    });
    print("External Wallet Payment:");
    print(response);
  }

  @override
  Widget build(BuildContext context) {
    final fullName = TextFormField(
      controller: _fullNameController,
      textInputAction: TextInputAction.next,
      obscureText: false,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Enter Full Name'),
      validator: (val) {
        if (val == null || val.isEmpty) return "Name cannot be empty";
        return null;
      },
    );
    final panName = TextFormField(
      controller: _panNameController,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      textInputAction: TextInputAction.next,
      obscureText: false,
      style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Enter Pan Card Name'),
    );
    final email = TextFormField(
      controller: _emailController,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      obscureText: false,
      style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Enter Email ID'),
      validator: (val) {
        if (val == null || val.isEmpty)
          return "Email cannot be empty";
        else if (!validateEmail(val)) return "Please Enter Valid Email Address";
        return null;
      },
    );
    final phone = TextFormField(
      controller: _phoneController,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      obscureText: false,
      style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Enter Phone Number'),
      validator: (val) {
        if (val == null || val.isEmpty) return "Phone Number cannot be empty";
        return null;
      },
    );
    final alterPhone = TextFormField(
      controller: _alterPhoneController,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      obscureText: false,
      style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Alternate Phone Number'),
    );
    final permanentAddress = TextFormField(
      maxLines: 2,
      controller: _permanentAddressController,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Permanent Address'),
      validator: (val) {
        if (val == null || val.isEmpty) return "Address cannot be empty";
        return null;
      },
    );
    final correspondingAddress = TextFormField(
      maxLines: 2,
      controller: _correspondingAddressController,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      textInputAction: TextInputAction.next,
      style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Corresponding Address'),
    );
    final executive = TextFormField(
      controller: _executiveController,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Created by Executive'),
    );
    final selectCity = DropdownButton(
        value: city.isEmpty ? null : city,
        items: [
          DropdownMenuItem(
            child: Text("   Nashik  "),
            value: "Nashik",
          ),
          DropdownMenuItem(
            child: Text("   Thane   "),
            value: "Thane",
          )
        ],
        hint: Text("  Select City  "),
        onChanged: (value) {
          setState(() {
            city = value;
          });
        });
    final password = TextFormField(
      controller: _passwordController,
      textInputAction: TextInputAction.next,
      obscureText: true,
      style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Password'),
      validator: (val) {
        if (val == null || val.isEmpty)
          return "Password cannot be empty";
        else if (!(val.length > 5))
          return "Password should be more then 5 characters";
        return null;
      },
    );
    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Colors.orange,
      child: MaterialButton(
        onPressed: () {
          if (_formKey.currentState.validate()) {
            if (city.isEmpty) {
              FlutterToast.showToast(
                  msg: "Please Select Your City",
                  toastLength: Toast.LENGTH_LONG);
            } else {
              generateRazorPayOrder();
            }
          }
        },
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        child: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Text(
                "SignUp",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 14.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
      ),
    );
    final invalidCredential = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: _isInvalidCredential
          ? [
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                child: Text(
                  '$alreadyExist already exist',
                  style: TextStyle(color: Color(0xffe0535b)),
                  textAlign: TextAlign.center,
                ),
              ),
            ]
          : [],
    );
    final futurePlan = FutureBuilder(
      future: getPlanList(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          default:
            if (snapshot.hasData) {
              return Center(child: showPlans(snapshot.data));
            } else {
              return Center(
                child: Text(
                    "Something Went Wrong.\nPlease try again after some time"),
              );
            }
        }
      },
    );
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 253, 235, 208),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Stack(
          children: <Widget>[
            Container(
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                      image: AssetImage("assets/back.webp"),
                      fit: BoxFit.cover)),
            ),
            Center(
              child: SingleChildScrollView(
                child: Card(
                  color: Color.fromARGB(230, 255, 255, 255),
                  margin: EdgeInsets.all(20),
                  elevation: 5,
                  child: Padding(
                      padding: EdgeInsets.all(30.0),
                      child: Center(
                        child: Form(
                          key: _formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              SizedBox(
                                height: 100.0,
                                child: Image.network(
                                  "https://vegi.co.in/static/media/logo11.ee474a7e.png",
                                  fit: BoxFit.contain,
                                ),
                              ),
                              SizedBox(height: 15.0),
                              fullName,
                              SizedBox(height: 15.0),
                              email,
                              SizedBox(height: 15.0),
                              phone,
                              SizedBox(height: 15.0),
                              alterPhone,
                              SizedBox(height: 15.0),
                              permanentAddress,
                              SizedBox(height: 15.0),
                              correspondingAddress,
                              selectCity,
                              SizedBox(height: 15.0),
                              panName,
                              SizedBox(height: 15.0),
                              executive,
                              SizedBox(height: 15.0),
                              password,
                              SizedBox(height: 5.0),
                              invalidCredential,
                              SizedBox(height: 50.0),
                              Text("Select Membership Below"),
                              futurePlan,
                              SizedBox(height: 15.0),
                              loginButton,
                              SizedBox(height: 15.0),
                              Text("Already Had an Account!,"),
                              InkWell(
                                  child: Text(
                                    "Click here to Log In",
                                    style: TextStyle(
                                        decoration: TextDecoration.underline,
                                        color: Colors.blue),
                                  ),
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  }),
                              SizedBox(height: 15.0),
                            ],
                          ),
                        ),
                      )),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
