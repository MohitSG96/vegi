import 'package:flutter/material.dart';
import 'package:grocery/models/item.dart';
import 'package:grocery/models/orderProduct.dart';
import 'package:grocery/models/previousOrder.dart';
import 'package:grocery/models/scoped_model/MainModel.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:grocery/utils/globals.dart' as globals;

class OrderDetails extends StatefulWidget {
  final PreviousOrder order;

  OrderDetails({Key key, @required this.order}) : super(key: key);

  @override
  _OrderDetailState createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetails> {
  PreviousOrder order;
  MainModel _model;
  List<Item> items;

  @override
  void initState() {
    super.initState();
    order = widget.order;
    _model = ScopedModel.of(context);
    items = _model.products;
  }

  showHeaderRow(val1, val2) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            "$val1:",
            style: TextStyle(
                fontSize: 20, color: Colors.red, fontWeight: FontWeight.w600),
          ),
          Text(
            "$val2",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
            overflow: TextOverflow.fade,
          ),
        ],
      ),
    );
  }

  showOrderItems() {
    List<OrderProduct> orders = order.products;
    double width = MediaQuery.of(context).size.width * 0.20;
    return ListView.builder(
      shrinkWrap: true,
      itemCount: orders.length,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        OrderProduct product = orders[index];
        int i = items
            .indexWhere((element) => element.id.trim() == product.id.trim());
        Item item = items[i];
        return Card(
          elevation: 3,
          child: ListTile(
            leading: Image.network(
              item.image != null && item.image.isNotEmpty
              ? globals.API_BASE + globals.PRODUCT_IMAGE + item.image
              : "https://vegi.co.in/static/media/logo.2989e92e.png",
              fit: BoxFit.fitWidth,
              width: width,
            ),
            title: Text("${product.title}"),
            subtitle: Text("Units: ${product.unit}"),
            trailing: Text("₹ ${product.price}"),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    DateTime dateTime = DateTime.tryParse(order.orderDateTime);
    String orderDate = "${dateTime.day}/${dateTime.month}/${dateTime.year}";
//    String orderTime = "${dateTime.hour}:${dateTime.minute}";
    return Scaffold(
      appBar: new AppBar(
        title: Text("Order Details"),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: EdgeInsets.only(top: 20, bottom: 10, left: 10, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                showHeaderRow("Order Id", order.id),
                SizedBox(height: 5),
                showHeaderRow("Status", order.status),
                SizedBox(height: 5),
                showHeaderRow("Payment Status", order.paymentStatus),
                SizedBox(height: 5),
                showHeaderRow("Address", order.deliveryAddress),
                SizedBox(height: 5),
                showHeaderRow("Order Date", orderDate),
                SizedBox(height: 5),
                order.deliveryTime != null &&
                        order.deliveryTime.trim().isNotEmpty
                    ? {
                        showHeaderRow("Delivery Time", order.deliveryTime),
                        SizedBox(height: 5)
                      }
                    : Center(),
                order.deliveryBoy != null && order.deliveryBoy.trim().isNotEmpty
                    ? {
                        showHeaderRow("Delivery Boy", order.deliveryBoy),
                        SizedBox(height: 5)
                      }
                    : Center(),
                showHeaderRow("Delivery Fee",
                    "${order.deliveryFee != 0.0 ? '₹ ${order.deliveryFee}' : 'Free'}"),
                SizedBox(height: 5),
                showHeaderRow(
                    "Total Amount", "₹ ${order.amount + order.deliveryFee}"),
                SizedBox(height: 10),
                showOrderItems(),
                SizedBox(height: 5),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
