import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grocery/pages/show_products.dart';
import 'package:grocery/pages/signup_screen.dart';
import 'package:grocery/utils/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginScreen> {
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _isInvalidCredential = false, _isLoading = false;
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
    _loadShared();
    AssetImage("assets/back.webp");
  }

  signIn(String userName, String password) async {
    _passwordController.text = "";
    setState(() {
      _isLoading = true;
    });
    var body = {
      "username": userName,
      "password": password,
    };
    http.Response response;
    try {
      response = await http.post(globals.API_BASE + globals.SIGNIN,
          headers: {"Content-Type": "application/json"},
          body: jsonEncode(body));
      if (response.statusCode == HttpStatus.ok) {
        var data = jsonDecode(response.body);
        if (!data["error"]) {
          var userData = data["data"];
          List<dynamic> permissions = userData["permissions"];
          var plans = userData["plan"];
          bool member = permissions.contains("member");
          sharedPreferences.setString("user_id", userData["_id"]);
          sharedPreferences.setString("user_name", userName);
          sharedPreferences.setString("name", userData["name"]);
          sharedPreferences.setString("phone", userData["phone"]);
          sharedPreferences.setString("address", userData["address"] ?? "");
          sharedPreferences.setString("email", userData["email"]);
          sharedPreferences.setString("token", userData["token"]);
          sharedPreferences.setString(
              "plan",
              userData["plan"] != null
                  ? userData["plan"]["category"] ?? ""
                  : "");
          sharedPreferences.setBool("member", member);
          sharedPreferences.setString("city", userData["city"] ?? "");
          setState(() {
            _isLoading = false;
            _isInvalidCredential = false;
          });
          globals.userName = _userNameController.text;
          globals.userId = userData["_id"];
          globals.userFullName = userData["name"];
          globals.userPhone = userData["phone"];
          globals.userAddress = userData["address"];
          globals.userEmail = userData["email"];
          globals.userToken = userData["token"];
          globals.plan = userData["plan"] != null
              ? userData["plan"]["category"] ?? ""
              : "";
          globals.member = member;
          if (userData["city"] != null &&
              userData["city"].toString().isNotEmpty)
            globals.city = userData["city"];
          globals.catId = "*";
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                  builder: (context) => ProductsList(
                        category: "All Product",
                      )),
              (route) => false);
        } else {
          setState(() {
            _isLoading = false;
            _isInvalidCredential = true;
          });
        }
      } else {
        var data = jsonDecode(response.body);
        var msg = "Something Went Wrong\nPlease try again after some time!";
        if (response.statusCode == HttpStatus.badRequest ||
            response.statusCode == HttpStatus.unauthorized) {
          var errors = data['errors'];
          for (var param in errors) {
            msg = param['msg'];
          }
          setState(() {
            _isLoading = false;
            _isInvalidCredential = true;
          });
        } else {
          print(response.body);
        }
        showDialog(
          context: context,
          builder: (BuildContext bc) {
            return AlertDialog(
              title: new Text('Error!'),
              content: Text('Error: ' + msg),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    } catch (ex) {
      print(ex);
    }
  }

  void _loadShared() async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool("welcome", false);
  }

  @override
  Widget build(BuildContext context) {
    final userName = TextFormField(
      controller: _userNameController,
      obscureText: false,
      style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'User Name'),
      validator: (val) {
        if (val == null || val.isEmpty) return "UserName cannot be empty";
        return null;
      },
    );
    final password = TextFormField(
      controller: _passwordController,
      obscureText: true,
      style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Password'),
      validator: (val) {
        if (val == null || val.isEmpty)
          return "Password cannot be empty";
        else if (!(val.length > 5))
          return "Password should be more then 5 characters";
        return null;
      },
    );
    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Colors.orange,
      child: MaterialButton(
        onPressed:
            (_userNameController.text != "" && _passwordController.text != "")
                ? () {
                    signIn(_userNameController.text, _passwordController.text);
                  }
                : null,
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        child: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Text(
                "Login",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 14.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
      ),
    );
    final invalidCredential = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: _isInvalidCredential
          ? [
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                child: Text(
                  'User Name or Password is incorrect.',
                  style: TextStyle(color: Color(0xffe0535b)),
                  textAlign: TextAlign.center,
                ),
              ),
            ]
          : [],
    );
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 253, 235, 208),
        body: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                          image: AssetImage("assets/back.webp"),
                          fit: BoxFit.cover)),
                ),
                Center(
                  child: SingleChildScrollView(
                      child: Card(
                    color: Color.fromARGB(225, 255, 255, 255),
                    margin: EdgeInsets.all(20),
                    elevation: 5,
                    child: Padding(
                        padding: EdgeInsets.all(30.0),
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              SizedBox(
                                height: 100.0,
                                child: Image.network(
                                  "https://vegi.co.in/static/media/logo11.ee474a7e.png",
                                  fit: BoxFit.contain,
                                ),
                              ),
                              SizedBox(height: 15.0),
                              userName,
                              SizedBox(height: 15.0),
                              password,
                              SizedBox(height: 5.0),
                              invalidCredential,
                              SizedBox(height: 50.0),
                              loginButton,
                              SizedBox(height: 15.0),
                              Text("Not Have an Account,"),
                              InkWell(
                                child: Text(
                                  "Click here for SignUp",
                                  style: TextStyle(
                                      decoration: TextDecoration.underline,
                                      color: Colors.blue),
                                ),
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => SignUpScreen()));
                                },
                              ),
                              SizedBox(height: 15.0),
                            ],
                          ),
                        )),
                  )),
                ),
              ],
            )));
  }
}
