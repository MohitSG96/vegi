import 'dart:convert';
import 'dart:io';

import 'package:grocery/models/item.dart';
import 'package:grocery/models/order.dart';
import 'package:grocery/utils/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:scoped_model/scoped_model.dart';

mixin Methods on Model {
  Order cart;
  List<Order> orders = [];
  List<Item> products = [];
  List<Item> cartItem = new List<Item>();
  static const String RAZOR_LIVE = "rzp_live_Jg2CTm5eCz3F6D";

  double cartAmount() {
    double _amount = 0.0;
    for (var cart in cartItem) _amount += cart.quantity * cart.price;
    return _amount;
  }

  int getFreeProductCount() {
    int freeProductCount = 0;
    for (Item cart in cartItem) {
      if (cart.price == 0) {
        freeProductCount++;
      }
    }
    return freeProductCount;
  }

  addToCart(Item item, price) {
    var i = cartItem.indexWhere((p) => p.id == item.id);
    if (i < 0) {
      item.price = double.tryParse(price.toString());
      item.quantity++;
      cartItem.add(item);
    } else {
      if (cartItem[i].price == 0) {
        Map<String, dynamic> price = item.prices[0];
        var cost = price["mumbaiPrice"].toString();
        if (globals.city.toLowerCase() == "nashik") {
          cost = price["nashikPrice"].toString();
        }
        cartItem[i].price = double.tryParse(cost);
        return;
      }
      cartItem[i].quantity++;
    }
    notifyListeners();
  }

  int getItemCount() {
    double count = 0;
    for (var cart in cartItem) {
      count += cart.quantity;
    }
    return count.toInt();
  }

  getOrders(amount) async {
    Map<String, dynamic> razorRes;
    var headers = {
      "Authorization": globals.userToken,
      "Content-Type": "application/json"
    };
    var data = {"amount": amount.toString()};
    try {
      var response = await http.post(
        globals.API_BASE + globals.RAZOR_ID,
        headers: headers,
        body: json.encode(data),
      );
      if (response.statusCode == HttpStatus.ok) {
        var resJson = json.decode(response.body);
        if (!resJson["error"]) {
          razorRes = resJson["data"];
          razorRes.addAll({"error": false});
        } else {
          var errors = resJson["errors"];
          razorRes = {"error": true};
          razorRes = errors["error"];
        }
      } else if (response.statusCode == HttpStatus.badRequest) {
        var resJson = json.decode(response.body);
        var errors = resJson["errors"];
        razorRes.addAll({"error": true, "param": errors["error"]["code"]});
        razorRes.addAll({"errors": errors});
      } else if (response.statusCode == HttpStatus.forbidden) {
        var resJson = json.decode(response.body);
        var errors = resJson["errors"];
        razorRes = errors[0];
        razorRes.addAll({"error": true});
      } else {
        var resJson = json.decode(response.body);
        var errors = resJson["errors"];
        razorRes = {
          "error": true,
          "message": errors[0],
        };
      }
    } catch (ex) {
      razorRes = {
        "error": true,
        "message":
            "Some Problem Occurred during Processing your Payment.\nPlease Try Again."
      };
    }
    return razorRes;
  }

  getPayment(amount, email, phone) async {
    Map<String, dynamic> razorRes = {};
    var headers = {
      "Authorization": globals.userToken,
      "Content-Type": "application/json"
    };
    var data = {
      "amount": amount.toString(),
      "email": email,
      "phone": phone,
    };
    try {
      var response = await http.post(
        globals.API_BASE + "/api/auth/razorpay",
        headers: headers,
        body: json.encode(data),
      );
      if (response.statusCode == HttpStatus.ok) {
        var resJson = json.decode(response.body);
        if (!resJson["error"]) {
          razorRes = resJson["data"];
          razorRes.addAll({"error": false});
        } else {
          var errors = resJson["errors"];
          razorRes = {"error": true};
          razorRes = errors["error"];
        }
      } else if (response.statusCode == HttpStatus.badRequest) {
        var resJson = json.decode(response.body);
        var errors = resJson["errors"];
        razorRes.addAll({"error": true, "param": errors["msg"]});
        razorRes.addAll({"errors": errors});
        razorRes.addAll({
          "title": "Validation Error",
          "message": errors["msg"],
        });
      } else if (response.statusCode == HttpStatus.forbidden) {
        var resJson = json.decode(response.body);
        var errors = resJson["errors"];
        razorRes = errors[0];
        razorRes.addAll({"error": true});
      } else {
        var resJson = json.decode(response.body);
        var errors = resJson["errors"];
        razorRes = {
          "error": true,
          "title": "Operation Not Allowes",
          "message": errors[0],
        };
      }
    } catch (ex) {
      razorRes = {
        "error": true,
        "title": "Something Went Wrong",
        "message":
            "Some Problem Occurred during Processing your Payment.\nPlease Try Again."
      };
    }
    return razorRes;
  }

  getProducts() async {
    Map<String, String> headers = {"Authorization": globals.userToken};
    try {
      var response =
          await http.get(globals.API_BASE + globals.PRODUCT, headers: headers);
      if (response.statusCode == HttpStatus.ok) {
        var resJson = json.decode(response.body);
        var error = resJson['error'];
        if (!error) {
          var productsMap = resJson['data'];
          products = [];
          for (Map<String, dynamic> product in productsMap) {
            if (product["title"] != null && product["slug"] != null) {
              products.add(Item.fromJson(product));
            }
          }
        }
      }
    } catch (ex) {
      print(ex);
    }
    return products;
  }

  orderFreeProducts(amount) async {
    var res;
    Map<String, String> headers = {
      "Authorization": globals.userToken,
      "Content-Type": "application/json"
    };
    Map<String, dynamic> data = new Map<String, dynamic>();
    data.addAll({"amount": cartAmount()});
    Map<String, dynamic> products = new Map<String, dynamic>();
    List<dynamic> item = new List<dynamic>();
    for (Item cart in cartItem) {
      item.add(cart.cartOrder());
    }
    products.addAll({"products": item});
    data.addAll(products);
    try {
      var response = await http.post(
        globals.API_BASE + globals.ADD_FREE_ORDER,
        headers: headers,
        body: jsonEncode(data),
      );
      if (response.statusCode == HttpStatus.ok) {
        var resJson = json.decode(response.body);
        print("Response: $resJson");
        if (!resJson["error"]) {
          res = resJson["data"];
          res.addAll({"error": false});
          cartItem.clear();
          cartItem = [];
        } else {
          var errors = resJson["errors"];
          res = {"error": true};
          res.addAll(errors["error"]);
        }
      } else if (response.statusCode == HttpStatus.badRequest) {
        var resJson = json.decode(response.body);
        var errors = resJson["errors"];
        res.addAll({"error": true, "param": errors["error"]["code"]});
        res.addAll({"errors": errors});
      } else if (response.statusCode == HttpStatus.forbidden) {
        var resJson = json.decode(response.body);
        var errors = resJson["errors"];
        res = errors[0];
        res.addAll({"error": true});
      } else {
        res = {
          "error": true,
          "message": "Something Went Wrong.\nPlease Try Again."
        };
      }
    } catch (ex) {
      res = {
        "error": true,
        "message": "Something Went Wrong.\nPlease Try Again."
      };
    }
    return res;
  }

  orderCart(razorResponse) async {
    var res;
    Map<String, String> headers = {
      "Authorization": globals.userToken,
      "Content-Type": "application/json"
    };
    Map<String, dynamic> data = new Map<String, dynamic>();
    data.addAll({"amount": cartAmount()});
    data.addAll({"response": razorResponse});
    Map<String, dynamic> products = new Map<String, dynamic>();
    List<dynamic> item = new List<dynamic>();
    for (Item cart in cartItem) {
      item.add(cart.cartOrder());
    }
    products.addAll({"products": item});
    data.addAll(products);
    try {
      var response = await http.post(
        globals.API_BASE + globals.ADD_ORDER,
        headers: headers,
        body: jsonEncode(data),
      );
      if (response.statusCode == HttpStatus.ok) {
        var resJson = json.decode(response.body);
        if (!resJson["error"]) {
          res = resJson["data"];
          res.addAll({"error": false});
          cartItem.clear();
          cartItem = [];
        } else {
          var errors = resJson["errors"];
          res = {"error": true};
          res.addAll(errors["error"]);
        }
      } else if (response.statusCode == HttpStatus.badRequest) {
        var resJson = json.decode(response.body);
        var errors = resJson["errors"];
        res.addAll({"error": true, "param": errors["error"]["code"]});
        res.addAll({"errors": errors});
      } else if (response.statusCode == HttpStatus.forbidden) {
        var resJson = json.decode(response.body);
        var errors = resJson["errors"];
        res = errors[0];
        res.addAll({"error": true});
      } else {
        res = {
          "error": true,
          "message": "Something Went Wrong.\nPlease Try Again."
        };
      }
    } catch (ex) {
      res = {
        "error": true,
        "message": "Something Went Wrong.\nPlease Try Again."
      };
    }
    return res;
  }

  searchProduct(String searchKey) async {
    List<Item> products = [];
    Map<String, dynamic> res = {"error": true, "data": products};
    Map<String, String> headers = {
      "Authorization": globals.userToken,
      "Content-Type": "application/json",
    };
    try {
      Map<String, dynamic> data = new Map<String, dynamic>();
      data.addAll({"searchKey": searchKey});
      var response = await http.post(
        globals.API_BASE + globals.SEARCH,
        headers: headers,
        body: jsonEncode(data),
      );
      if (response.statusCode == HttpStatus.ok) {
        var resJson = json.decode(response.body);
        var error = resJson['error'];
        if (!error) {
          var productsMap = resJson['data'];
          products = [];
          for (Map<String, dynamic> product in productsMap) {
            if (product["title"] != null && product["slug"] != null) {
              products.add(Item.fromJson(product));
            }
          }
          res["error"] = error;
          res["data"] = products;
        }
      } else {
        var resJson = json.decode(response.body);
        var error = resJson['error'];
        res["error"] = error;
        res.addAll({"data": resJson['data']});
      }
    } catch (ex) {
      print(ex);
    }
    return res;
  }
}
