class Category {
  String title;
  String type;
  String image;
  String slug;
  List<dynamic> children;
  String id;
  int freeProducts;
  bool isForFree;

  Category({
    this.id,
    this.title,
    this.type,
    this.image,
    this.slug,
    this.children,
    this.freeProducts,
    this.isForFree,
  });

  Map<String, dynamic> toJson(bool image) => {
        "_id": id,
        "title": title,
        "name": type,
        "image": image,
        "price": slug,
        "children": children,
        "freeProducts": freeProducts,
      };

  factory Category.fromJson(Map<String, dynamic> parsedJson) {
    return Category(
      id: parsedJson['_id'],
      title: parsedJson['title'],
      type: parsedJson['name'],
      slug: parsedJson['slug'],
      image: parsedJson['icon'] != null ? parsedJson['icon'] : "",
      children: parsedJson['children'],
      freeProducts: parsedJson['freeProducts'] ?? 0,
      isForFree: parsedJson['isForFree'] != null &&
              parsedJson['isForFree'].toString().isNotEmpty
          ? parsedJson['isForFree'].toString().contains("true") ? true : false
          : false,
    );
  }
}
