class Order {
  String id;
  String currency;
  int amount;

  Order({this.id, this.currency, this.amount});

  factory Order.fromJson(Map<String, dynamic> parsedJson) {
    return Order(
        amount: parsedJson['amount'],
        currency: parsedJson['currency'],
        id: parsedJson['id']);
  }
}
