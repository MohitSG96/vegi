import 'package:grocery/models/orderProduct.dart';

class PreviousOrder {
  double amount, deliveryFee;
  String status, orderId, paymentId, paymentStatus, deliveryTime, deliveryBoy;
  String deliveryAddress, date, orderDateTime, id;
  List<OrderProduct> products;

  PreviousOrder(
      {this.id,
      this.amount,
      this.status,
      this.orderId,
      this.paymentId,
      this.paymentStatus,
      this.deliveryTime,
      this.deliveryBoy,
      this.deliveryFee,
      this.deliveryAddress,
      this.date,
      this.orderDateTime,
      this.products});

  factory PreviousOrder.fromJson(Map<String, dynamic> parsedJson, double total, List<OrderProduct> parsedProducts) {
    return PreviousOrder(
      id: parsedJson['_id'],
      amount: total,
      status: parsedJson['status'] == null ? "No Status" : parsedJson['status'],
      orderId: parsedJson['orderId'],
      paymentId: parsedJson['paymentId'] == null ? "" : parsedJson['paymentId'],
      paymentStatus: parsedJson['paymentStatus'] == null
          ? ""
          : parsedJson['paymentStatus'],
      deliveryTime:
          parsedJson['deliveryTime'] == null ? "" : parsedJson['deliveryTime'],
      deliveryBoy:
          parsedJson['deliveryBoy'] == null ? "" : parsedJson['deliveryBoy'],
      deliveryFee: parsedJson['deliveryFee'] == null
          ? 0.0
          : double.tryParse(parsedJson['deliveryFee'].toString()),
      deliveryAddress: parsedJson['deliveryAddress'] == null
          ? ""
          : parsedJson['deliveryAddress'],
      date: parsedJson['date'] == null ? "" : parsedJson['date'],
      orderDateTime: parsedJson['createdAt'],
      products: parsedProducts,
    );
  }
}
