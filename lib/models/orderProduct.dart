class OrderProduct {
  String id, title, idProduct;
  int unit;
  double price;

  OrderProduct({this.id, this.title, this.idProduct, this.unit, this.price});

  factory OrderProduct.fromJson(Map<String, dynamic> parsedJson) {
    return OrderProduct(
      id: parsedJson['item']['_id'],
      title: parsedJson['item']['title'],
      unit: int.tryParse(parsedJson['unit'].toString()),
      price: double.tryParse(parsedJson['price'].toString()),
      idProduct: parsedJson['_id'],
    );
  }
}
