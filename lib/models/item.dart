import 'package:grocery/utils/globals.dart' as globals;

class Item {
  String id;
  String name;
  String image;
  double price;
  double quantity;
  int unit;
  String measurement;
  double discount;
  String catId;
  String slug;
  List<dynamic> prices;

  Item(
      {this.id,
      this.name,
      this.image,
      this.prices,
      this.quantity,
      this.unit,
      this.measurement,
      this.discount,
      this.catId,
      this.slug});

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "image": image,
        "prices": prices,
        "quantity": quantity,
        "unit": unit,
        "measurementType": measurement,
        "slug": slug,
        "category": {"_id": catId},
      };

  Map<String, dynamic> cartOrder() => {
        "unit": quantity,
        "price": price,
        "item": {
          "_id": id,
          "title": name,
          "image": image,
          "unit": unit,
          "slug": slug,
          "category": {"_id": catId},
        },
        "measurementType": measurement,
      };

  factory Item.fromJson(Map<String, dynamic> parsedJson) {
    return Item(
      id: parsedJson['_id'],
      name: parsedJson['title'],
      prices: parsedJson['prices'],
      quantity: parsedJson['quantity'] == null
          ? 0.0
          : double.tryParse(parsedJson['quantity'].toString()),
      unit: parsedJson['unit'],
      image: parsedJson['image'],
      catId: parsedJson["category"]["_id"],
      slug: parsedJson["slug"],
    );
  }
}
