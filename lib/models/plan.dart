class Plan {
  String id, title, image, slug, catId;
  int weeklyMumbaiPrice, monthlyMumbaiPrice;
  int weeklyNashilPrice, monthlyNashikPrice;
  String description, subDescription, created;

  Plan({
    this.id,
    this.slug,
    this.title,
    this.image,
    this.description,
    this.subDescription,
    this.created,
    this.weeklyMumbaiPrice,
    this.monthlyMumbaiPrice,
    this.monthlyNashikPrice,
    this.weeklyNashilPrice,
    this.catId,
  });

  factory Plan.fromJson(Map<String, dynamic> parsedJson) {
    return Plan(
      id: parsedJson['_id'],
      slug: parsedJson['slug'] ?? "",
      title: parsedJson['title'] ?? "",
      image: parsedJson['image'] ?? "",
      catId: parsedJson['category'] != null ? parsedJson['category']['id'] : "",
      weeklyMumbaiPrice: parsedJson['mumbaiWeeklyPrice'] ?? 0,
      monthlyMumbaiPrice: parsedJson['mumbaiMonthlyPrice'] ?? 0 ,
      weeklyNashilPrice: parsedJson['nashikWeeklyPrice'] ?? 0,
      monthlyNashikPrice: parsedJson['nashikMonthlyPrice'] ?? 0,
      description: parsedJson['description'] ?? "",
      subDescription: parsedJson['subDescription'] ?? "",
      created: parsedJson['createdAt'],
    );
  }
}
